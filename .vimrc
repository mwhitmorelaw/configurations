set ts=2 sw=2
syntax on " Syntax highlighting
set showmatch " Shows matching brackets
set ruler " Always shows location in file (line#)
set smarttab " Autotabs for certain code
set expandtab
set shell=C:\WINDOWS\System32\WindowsPowerShell\v1.0\powershell.exe
"set autoindent
